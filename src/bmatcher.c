#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ALPHABET_LEN 256

void computeLastOccurrence(int *J, char *pattern, int patternLength)
{
    for (int i = 0; i < ALPHABET_LEN; i++)
        J[i] = patternLength;

    for (int i = 0; i < patternLength-1; i++)
        J[(int)pattern[i]] = patternLength-1 - i;
}

int BMatcher(char *text, int textLength, char *pattern, int patternLength)
{
    int x, i, shift;
    int J[ALPHABET_LEN];
    computeLastOccurrence(J, pattern, patternLength);

    i = patternLength-1;
    while (i < textLength)
    {
        int j = patternLength-1;
        while (j >= 0 && (text[i] == pattern[j]))
        {
            --i;
            --j;
        }
        if (j < 0)
        {
            shift = i+1;
            for (x = 0; x < textLength; x++)
            {
                if (x == shift)
                    printf("\x1b[31;1m");
                else if (x == shift + patternLength)
                {
                    printf("\x1b[0m");
                    break;
                }
                printf("%c", text[x]);
            }
            x += BMatcher(text + shift + patternLength, strlen(text + shift + patternLength), pattern, patternLength);
            return x;
        }
        i += J[(int)text[i]];
    }
    return 0;
}

