#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int BMatcher(char *text, int textLength, char *pattern, int patternLength);

int main(int argc, char *argv[])
{
    if (argc <= 1)
        printf("Enter pattern.\n");
    else if (argc == 2)
        printf("Enter filename.\n");
    else if (argc == 3)
    {
        char *pattern = argv[1];
        int patternLength = strlen(pattern);
        pattern[patternLength] = '\0';
        FILE *input;
        input = fopen(argv[2], "r");
        int textLength = 0;
        while (!feof(input))
        {
            fgetc(input);
            textLength++;
        }
        if (patternLength > textLength)
        {
            printf("Pattern is bigger than text.\n");
            return -1;
        }
        char *text = (char*)calloc(textLength, sizeof(char));
        rewind(input);
        int i = 0;
        while (!feof(input))
        {
            fscanf(input, "%c", &text[i]);
            i++;
        }
        fclose(input);
        printf("\n");
        int lastSym = BMatcher(text, textLength, pattern, patternLength);
        while (lastSym < textLength)
        {
            printf("%c", text[lastSym]);
            lastSym++;
        }
        return 0;
    }
    else if (argc > 3)
        printf("Too many arguments.\n");
    return -1;
}
